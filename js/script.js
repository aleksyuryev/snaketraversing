let arr1 = [ [1,2,3], [4,5,6] ];
let arr2 = [ [1,2,3], [4,5,6], [7,8,9] ];
let arr3 = [ [1,2,3], [4,5,6], [7,8,9], [10,11,12] ];
let arr4 = [ [1,2,3,4], [5,6,7,8], [9,10,11,12], [13,14,15,16] ];

function theFunction(arr, dir) {
	for ( let y = 0; y < arr.length*arr[0].length; y++ ) {

		( ((Math.floor( y / arr[0].length ) % 2 === 0) && Boolean(dir))
			&&
				console.log( `arr[${Math.floor(y / arr[0].length)}][${y % arr[0].length}] = [${arr[Math.floor(y / arr[0].length)][y % arr[0].length]}]` )
			)
		||
		( ((Math.floor( y / arr[0].length ) % 2 !== 0) && Boolean(dir))
			&&
				console.log( `arr[${Math.floor(y / arr[0].length)}][${arr[0].length - 1 - ( y % arr[0].length )}] = [${arr[Math.floor(y / arr[0].length)][arr[0].length - 1 - ( y % arr[0].length )]}]` )
		)
		||
		( ( (Math.floor( y / arr[0].length ) % 2 === 0) && !Boolean(dir) )
			&&
				console.log( `arr[${Math.floor(y / arr[0].length)}][${arr[0].length - 1 - ( y % arr[0].length )}] = [${arr[Math.floor(y / arr[0].length)][arr[0].length - 1 - ( y % arr[0].length )]}]` )
			)
		||
		( ((Math.floor( y / arr[0].length ) % 2 !== 0) && !Boolean(dir))
			&&
				console.log( `arr[${Math.floor(y / arr[0].length)}][${y % arr[0].length}] = [${arr[Math.floor(y / arr[0].length)][y % arr[0].length]}]` )
		);

	}
}

theFunction(arr1, 1);